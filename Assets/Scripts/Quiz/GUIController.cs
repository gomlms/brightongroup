﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIController : MonoBehaviour
{
  private GameObject consoleQuiz;
  public GameObject title;
  public GameObject content;
  public GameObject answer1;
  public GameObject answer2;
  public GameObject answer3;
  public GameObject answer4;

  private Question question;

  // Start is called before the first frame update
  void Start(){
    if(consoleQuiz == null){
      consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");
      question = consoleQuiz.GetComponent<ConsoleQuizController>().currentQuestions[consoleQuiz.GetComponent<ConsoleQuizController>().currentQuestionIndex];

      title.GetComponent<TMPro.TextMeshPro>().text = question.GetTitle();
      content.GetComponent<TMPro.TextMeshPro>().text = question.GetContent();

      answer1.GetComponent<TMPro.TextMeshPro>().text = question.GetChoices()[0].ToString();
      answer1.GetComponent<AnswerController>().questionText = question.GetChoices()[0].ToString();

      answer2.GetComponent<TMPro.TextMeshPro>().text = question.GetChoices()[1].ToString();
      answer2.GetComponent<AnswerController>().questionText = question.GetChoices()[1].ToString();

      answer3.GetComponent<TMPro.TextMeshPro>().text = question.GetChoices()[2].ToString();
      answer3.GetComponent<AnswerController>().questionText = question.GetChoices()[2].ToString();

      answer4.GetComponent<TMPro.TextMeshPro>().text = question.GetChoices()[3].ToString();
      answer4.GetComponent<AnswerController>().questionText = question.GetChoices()[3].ToString();
    }
  }

  // Update is called once per frame
  void Update(){
      
  }

  public void checkAnswer(string answer){
    Debug.Log(answer);
    Debug.Log(question.GetAnswer());
    if(answer.Equals(question.GetAnswer())){
      Debug.Log("Correct Answer Selected");

      consoleQuiz.GetComponent<ConsoleQuizController>().lastAnswerCorrect = true;
    } else {
      Debug.Log("Incorrect Answer Selected");

      consoleQuiz.GetComponent<ConsoleQuizController>().lastAnswerCorrect = false;
    }

    GoToShip();
  }

  void GoToShip(){
    SceneManager.LoadScene("Main");
  }
}
