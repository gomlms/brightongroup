﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LessonMessage : MonoBehaviour
{
    public float waitTime;

    // Start is called before the first frame update
    void Start()
    {
        GameObject consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");

        if(!consoleQuiz.GetComponent<ConsoleQuizController>().fromLesson){
            gameObject.SetActive(false);
        } else {
            consoleQuiz.GetComponent<ConsoleQuizController>().fromLesson = false;
            StartCoroutine(DisableMessage());
        }
    }

    IEnumerator DisableMessage(){
        yield return new WaitForSeconds(waitTime);

        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
