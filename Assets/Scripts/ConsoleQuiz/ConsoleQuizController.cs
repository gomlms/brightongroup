﻿using System.IO;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConsoleQuizController : MonoBehaviour
{
    private string quizDataFileName = "quizData.json";
    private string lessonDataFileName = "lessonData.json";
    private JsonLoader jsonLoader = new JsonLoader();

    public List<Question> currentQuestions;
    public int currentQuestionIndex = 0;
    private Lesson currentLesson;
    
    private bool DEBUG = true;

    public bool lastAnswerCorrect = true;
    public bool firstInstantiated;

    public bool room1Status = true;
    public bool room2Status = true;
    public bool room3Status = true;

    public bool enemyPresent = false;

    public GameObject room1;
    public GameObject room2;
    public GameObject room3;
    public GameObject ship;

    public GameObject healthSlider;

    public int damage;

    public bool fromLesson = false;

    public int lessonNum;

    public void Awake(){
        DontDestroyOnLoad(this);
        firstInstantiated = true;
    }

    void Start(){
        
    }

    void OnLevelWasLoaded(int level){
        room1 = GameObject.FindGameObjectWithTag("Room1");
        room2 = GameObject.FindGameObjectWithTag("Room2");
        room3 = GameObject.FindGameObjectWithTag("Room3");

        float rand = Random.Range(1,3);
        if (rand == 1){
            enemyPresent = true;
        } else {
            enemyPresent = false;
        }

        if(fromLesson || firstInstantiated){
            enemyPresent = false;
        }
        
        // Heal player if they passed the lesson
        if(fromLesson){
            gameObject.GetComponent<PlayerStats>().health += damage;
        }

        if(level == 1 && !fromLesson){
            if(!lastAnswerCorrect && room1 != null){
                gameObject.GetComponent<PlayerStats>().health -= damage;
                lastAnswerCorrect = true;

                if(gameObject.GetComponent<PlayerStats>().health <= 0){
                    // Kill Player (Load Game Over or something)
                }
            }

            healthSlider = GameObject.FindGameObjectWithTag("HealthSlider");
            healthSlider.GetComponent<Slider>().value = gameObject.GetComponent<PlayerStats>().health;
        }
    }

    /**
     * Loads the question data into a List<Question> and returns to caller.
     **/
    public void LoadQuestions(string subject, string level)
    {
        currentQuestions = jsonLoader.LoadQuestions(subject, level);

        room1Status = room1.GetComponent<Room>().operational;
        room2Status = room2.GetComponent<Room>().operational;
        room3Status = room3.GetComponent<Room>().operational;

        SceneManager.LoadScene("Quiz");
    }

    /**
     * Load the Lesson 
     **/
     public void LoadLesson(string subject, string level)
    {
        currentLesson = jsonLoader.LoadLesson(subject, level);


    }
}

public class Question
{
    private string title;
    private string content;
    private List<string> choices;
    private string answer;

    public void SetTitle(string title)
    {
        this.title = title;
    }

    public string GetTitle() { return this.title; }

    public void SetContent(string content)
    {
        this.content = content;
    }

    public string GetContent() { return this.content; }

    public void SetChoices(JArray choices)
    {
        this.choices = choices.ToObject<List<string>>();
    }

    public List<string> GetChoices() { return this.choices; }

    public void SetAnswer(string answer)
    {
        this.answer = answer;
    }

    public string GetAnswer() { return this.answer; }

}

public class Lesson
{
    private string title;
    private List<string> content = new List<string>();
    private List<Question> questionList = new List<Question>();

    public void SetTitle(string title)
    {
        this.title = title;
    }

    public string GetTitle() { return this.title; }

    public void SetContent(JArray content)
    {
        this.content = content.ToObject<List<string>>(); ;
    }

    public List<string> GetContent() { return this.content; }

    public void AddQuestions(Question question)
    {
        this.questionList.Add(question);
    }

    public List<Question> GetQuestions()
    {
        return this.questionList;
    }

}