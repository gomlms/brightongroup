﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{

    public Slider Volume;
    public AudioSource myMusic;

    // Update is called once per frame
    void Update()
    {
        if(myMusic && Volume){
            myMusic.volume = Volume.value;
        }
    }
}
