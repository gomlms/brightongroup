﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    private Vector2 moveVelocity;
    private bool facingRight = true;

    private Rigidbody2D rb;
    private Animator animator;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.freezeRotation = true;
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        // Get input and set the moveVelocity
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        moveVelocity = moveInput.normalized * speed;

        if (Mathf.Abs(moveVelocity.x) > 0)
        {
            animator.SetFloat("Speed", Mathf.Abs(moveVelocity.x));
        }
        else if (Mathf.Abs(moveVelocity.y) > 0)
        {
            animator.SetFloat("Speed", Mathf.Abs(moveVelocity.y));
        }
        else
        {
            animator.SetFloat("Speed", 0.0f);
        }
    }

    private void FixedUpdate()
    {
        // Rotate the player to face direction of movement
        if (Input.GetAxisRaw("Horizontal") > 0 && !facingRight)
        {
            //flip();

        } else if (Input.GetAxisRaw("Horizontal") < 0 && facingRight)
        {
            //flip();
        }

        // Move the player
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    void flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

  void OnTriggerEnter2D(Collider2D col){
    
  }
}
