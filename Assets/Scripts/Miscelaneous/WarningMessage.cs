﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningMessage : MonoBehaviour
{
    public float waitTime;

    // Start is called before the first frame update
    void Start()
    {
        GameObject consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");

        if(!consoleQuiz.GetComponent<ConsoleQuizController>().enemyPresent){
            gameObject.SetActive(false);
        } else {
            StartCoroutine(DisableMessage());
        }
    }

    IEnumerator DisableMessage(){
        yield return new WaitForSeconds(waitTime);

        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
