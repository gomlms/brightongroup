﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{

    private Queue<String> instructionQueue;
    private List<Question> quiz;

    [TextArea(3, 10)]
    public string[] intructions;

    private Lesson lesson;
    private Question currentQuestion;

    private Sprite[] slideList;
    public GameObject lessonSlide;

    public Boolean isLesson = false;

    private int phase = 0;
    private int coinCount = 0;
    private int lessonSlideNum = 0;
    private int questionNum = 0;
    private string guess = "";
    private bool inLesson;
    private bool inQuiz;

    public Text dialogueText;
    public GameObject coin;
    public Text coinTextbox;
    public GameObject upgradeArrow;
    public GameObject lessonBox;
    public Text lessonText;
    public GameObject lessonNextButton;
    public GameObject lessonPrevButton;
    public GameObject contentBox;
    public GameObject contentNextButton;
    public GameObject fire;
    public GameObject optionsButtons;
    public GameObject optionText;
    public Text option1Text;
    public Text option2Text;
    public Text option3Text;
    public Text option4Text;
    public Sprite buttonOffSprite;
    public Sprite buttonOnSprite;
    public GameObject submitButton;
    public GameObject clearButton;

    private JsonLoader jsonLoader = new JsonLoader();

    // Start is called before the first frame update
    void Start()
    {
        coin.SetActive(false);
        upgradeArrow.SetActive(false);
        lessonNextButton.SetActive(false);
        lessonPrevButton.SetActive(false);
        fire.SetActive(false);
        optionsButtons.SetActive(false);
        optionText.SetActive(false);
        submitButton.SetActive(false);
        clearButton.SetActive(false);
        instructionQueue = new Queue<string>();

        lessonSlide.SetActive(false);

        lessonText.text = "";
        dialogueText.text = "";

        inLesson = false;
        inQuiz = false;

        foreach (string instruction in intructions)
        {
            instructionQueue.Enqueue(instruction);
        }

        StartCoroutine(WaitSeconds(3.0f));
        DisplayNextInstruction();
    }

    private void Update()
    {
        coinTextbox.text = coinCount.ToString();
    }

    public void DisplayNextInstruction()
    {

        if (instructionQueue.Count == 0)
        {
            EndTutorial();
            return;
        }

        switch (phase)
        {
            case 2:
                PlayReceiveCoinAnimation();
                break;

            case 3:
                coin.SetActive(false);
                upgradeArrow.SetActive(true);
                contentNextButton.SetActive(false);
                break;

            case 7:
                fire.SetActive(true);
                contentNextButton.SetActive(false);
                break;
        }

        string instruction = instructionQueue.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeInstruction(instruction));

        phase++;

    }
    
    private void EndTutorial()
    {
        dialogueText.text = "";

        SceneManager.LoadScene("Main");
    }

    private void PlayReceiveCoinAnimation()
    {
        coin.SetActive(true);

        coinCount++;

        coinTextbox.text = coinCount.ToString();
    }

    public void UpgradeRoom()
    {
        upgradeArrow.SetActive(false);

        if (coinCount > 0)
        {
            Debug.Log("Upgrade");

            coinCount--;
        }

        PlayLesson();
    }

    public void PlayLesson()
    {
        dialogueText.text = "";

        inLesson = true;

        contentBox.GetComponent<Animator>().SetBool("Active", false);
        lessonBox.GetComponent<Animator>().SetBool("Active", true);

        StartCoroutine(WaitSeconds(3.0f));

        lessonNextButton.SetActive(true);
        lessonPrevButton.SetActive(true);

        Debug.Log("In leeson");

        lesson = jsonLoader.LoadLesson("tutorial", "1");

        slideList = null;

        slideList = Resources.LoadAll<Sprite>("Slides/tutorial/1");

        lessonSlide.SetActive(true);

        DisplayNextLesson();
    }

    public void DisplayNextLesson()
    {
        if (lessonSlideNum < lesson.GetContent().Count)
        {
            lessonSlide.GetComponent<Image>().sprite = slideList[lessonSlideNum];
            lessonSlideNum++;
        }
        else if (lessonSlideNum == lesson.GetContent().Count)
        {
            lessonSlide.SetActive(false);
            PlayLessonQuiz();
        }
    }

    public void DisplayPrevLesson()
    {
        if (lessonSlideNum >= 0)
        {
            lessonSlideNum--;
            lessonSlide.GetComponent<Image>().sprite = slideList[lessonSlideNum];
        }
    }

    private void PlayLessonQuiz()
    {
        Debug.Log("IN LESSON QUIZ");
        lessonNextButton.SetActive(false);
        lessonPrevButton.SetActive(false);
        optionsButtons.SetActive(true);
        optionText.SetActive(true);
        submitButton.SetActive(true);
        clearButton.SetActive(true);

        quiz = new List<Question>();
        quiz.Clear();
        questionNum = 0;

        foreach (Question ques in lesson.GetQuestions())
        {
            quiz.Add(ques);
        }

        ClearAnswers();
        NextQuesion();
    }

    private void NextQuesion()
    {
        if (questionNum == quiz.Count)
        {
            if (inLesson)
            {
                ExitLesson();
                return;
            }
            else if (inQuiz)
            {
                ExitQuiz();
                return;
            }
        }

        ClearAnswers();

        currentQuestion = quiz[questionNum];

        StopAllCoroutines();
        StartCoroutine(TypeLessonContent(currentQuestion.GetContent()));

        option1Text.text = currentQuestion.GetChoices()[0].ToString();
        option2Text.text = currentQuestion.GetChoices()[1].ToString();
        option3Text.text = currentQuestion.GetChoices()[2].ToString();
        option4Text.text = currentQuestion.GetChoices()[3].ToString();
    }

    private void ExitLesson()
    {
        lessonText.text = "";
        inLesson = false;

        contentBox.GetComponent<Animator>().SetBool("Active", true);
        lessonBox.GetComponent<Animator>().SetBool("Active", false);

        optionsButtons.SetActive(false);
        submitButton.SetActive(false);
        clearButton.SetActive(false);
        optionText.SetActive(false);
        contentNextButton.SetActive(true);

        DisplayNextInstruction();
    }

    public void PickOption1()
    {
        ClearAnswers();

        GameObject optionBtn = GameObject.Find(EventSystem.current.currentSelectedGameObject.name);
        optionBtn.GetComponent<Image>().sprite = buttonOnSprite;

        guess = currentQuestion.GetChoices()[0];

        Debug.Log("Button clicked: " + guess);
    }

    public void PickOption2()
    {
        ClearAnswers();

        GameObject optionBtn = GameObject.Find(EventSystem.current.currentSelectedGameObject.name);
        optionBtn.GetComponent<Image>().sprite = buttonOnSprite;

        guess = currentQuestion.GetChoices()[1];

        Debug.Log("Button clicked: " + guess);
    }

    public void PickOption3()
    {
        ClearAnswers();

        GameObject optionBtn = GameObject.Find(EventSystem.current.currentSelectedGameObject.name);
        optionBtn.GetComponent<Image>().sprite = buttonOnSprite;

        guess = currentQuestion.GetChoices()[2];

        Debug.Log("Button clicked: " + guess);
    }

    public void PickOption4()
    {
        ClearAnswers();

        GameObject optionBtn = GameObject.Find(EventSystem.current.currentSelectedGameObject.name);
        optionBtn.GetComponent<Image>().sprite = buttonOnSprite;

        guess = currentQuestion.GetChoices()[3];

        Debug.Log("Button clicked: " + guess);
    }

    public void ClearAnswers()
    {
        guess = "";

        for (int x = 1; x <= 4; x++)
        {
            GameObject.Find("Option" + (x).ToString()).GetComponent<Image>().sprite = buttonOffSprite;
        }

    }

    public void CheckAnswer()
    {
        if (guess == currentQuestion.GetAnswer())
        {
            StartCoroutine(TypeLessonContent("Correct!"));
            StartCoroutine(WaitSeconds(5.0f));

            ClearAnswers();
            questionNum++;
            NextQuesion();
            return;
        }
        else
        {
            StartCoroutine(TypeLessonContent("Incorrect! Try again..."));
            StartCoroutine(WaitSeconds(5.0f));

            Debug.Log("Lesson Ans is: " + currentQuestion.GetAnswer());
            Debug.Log("Chosen ans is: " + guess);

            ClearAnswers();
            NextQuesion();
            return;
        }
    }

    public void StartQuiz()
    {
        dialogueText.text = "";
        inQuiz = true;

        Debug.Log("Start quiz");
        contentBox.GetComponent<Animator>().SetBool("Active", false);
        lessonBox.GetComponent<Animator>().SetBool("Active", true);

        StartCoroutine(WaitSeconds(2.0f));

        optionsButtons.SetActive(true);
        optionText.SetActive(true);
        submitButton.SetActive(true);
        clearButton.SetActive(true);

        quiz = new List<Question>();
        quiz.Clear();
        quiz = jsonLoader.LoadQuestions("tutorial", "1");

        questionNum = 0;

        ClearAnswers();
        NextQuesion();
    }

    private void ExitQuiz()
    {
        lessonText.text = "";
        inQuiz = false;

        contentBox.GetComponent<Animator>().SetBool("Active", true);
        lessonBox.GetComponent<Animator>().SetBool("Active", false);

        optionsButtons.SetActive(false);
        submitButton.SetActive(false);
        clearButton.SetActive(false);
        optionText.SetActive(false);
        fire.SetActive(false);
        contentNextButton.SetActive(true);

        DisplayNextInstruction();
    }

    public IEnumerator WaitSeconds(float seconds)
    {
        Debug.Log("Start waiting for " + seconds + " seconds");
        yield return new WaitForSeconds(seconds);
        Debug.Log("Finished waiting for "+seconds+" seconds");
    }

    public IEnumerator TypeInstruction(string instruction)
    {
        dialogueText.text = "";

        foreach (char letter in instruction.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    public  IEnumerator TypeLessonContent(string lesson)
    {
        lessonText.text = "";

        foreach (char letter in lesson.ToCharArray())
        {
            lessonText.text += letter;
            yield return null;
        }
    }
}
