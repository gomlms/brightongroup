﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float timerToBreak;
    public GameObject room1;
    public GameObject room2;
    public GameObject room3;

    public bool allRoomsOperational;

    public GameObject consoleQuiz;

    // Start is called before the first frame update
    void Start(){
        
        allRoomsOperational = true;
        timerToBreak = Mathf.Round(Random.Range(5, 10));
        
        consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");
        room1 = GameObject.FindGameObjectWithTag("Room1");
        room2 = GameObject.FindGameObjectWithTag("Room2");
        room3 = GameObject.FindGameObjectWithTag("Room3");

        if(!consoleQuiz.GetComponent<ConsoleQuizController>().enemyPresent){
            gameObject.SetActive(false);
        } else {
            InvokeRepeating("CheckTimer", 0, 1f);
        }       
    }

    // Update is called once per frame
    void Update(){
        
    }

    void CheckTimer() {
        if(timerToBreak <= 0){
            int roomIndex = Mathf.RoundToInt(Random.Range(1,3));
            switch(roomIndex){
                case 1:
                    room1.GetComponent<Room>().operational = false;
                    break;
                case 2:
                    room2.GetComponent<Room>().operational = false;
                    break;
                case 3:
                    room3.GetComponent<Room>().operational = false;
                    break;
            }

            timerToBreak = Mathf.Round(Random.Range(15, 30));
            allRoomsOperational = false;
        }

        if(allRoomsOperational){
            timerToBreak -= 1;
        }
    }

    void CheckRoomsOperational(){
        
    }
}
