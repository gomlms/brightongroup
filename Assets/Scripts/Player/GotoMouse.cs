﻿using UnityEngine;
using System.Collections;

public class GotoMouse : MonoBehaviour
{

    public float speed = 1.5f;
    private Vector3 target;

    private bool facingRight = true;

    void Start()
    {
        target = transform.position;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            target.z = transform.position.z;
        }

        if (target.z > transform.position.z && facingRight)
        {
            flip();
        }
        else if (target.z < transform.position.z && !facingRight)
        {
            flip();
        }

        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
    }

    void flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}