﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System.IO;

public class JsonLoader : MonoBehaviour
{

    private string quizDataFileName = "quizData.json";
    private string lessonDataFileName = "lessonData.json";

    /**
     * Loads the quiz and returns a JObject to caller
     * */
    private JObject LoadQuizData()
    {
        JObject quiz;

        string filePath = Path.Combine(Application.streamingAssetsPath, quizDataFileName);

        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            string dataAsJson = File.ReadAllText(filePath);

            quiz = JObject.Parse(dataAsJson);

        }
        else
        {
            quiz = null;
            Debug.LogError("Cannot load quiz data!");
        }

        return quiz;
    }

    /**
     * Loads the question data into a List<Question> and returns to caller.
     * */
    public List<Question> LoadQuestions(string subject, string level)
    {
        Debug.Log("Starting Console with Subject: " + subject + " and Level: " + level);
        // Load Data from JSON with subject and level
        JObject quiz = LoadQuizData();

        int rand = (int)Mathf.Round(Random.Range(1,3));
        switch(rand){
            case 1:
                level = "1";
                break;
            case 2:
                level = "2";
                break;
            case 3:
                level = "3";
                break;
        }

        // Find the subject and level, load them into a JArray
        JArray allQuestions = (JArray)quiz["quiz"][subject][level];

        List<Question> questions = new List<Question>();

        for (int x = 0; x < allQuestions.Count; x++)
        {
            Question ques = new Question();

            ques.SetTitle(allQuestions[x]["title"].ToString());
            ques.SetContent(allQuestions[x]["content"].ToString());

            JArray choices = (JArray)allQuestions[x]["choices"];
            ques.SetChoices(choices);

            ques.SetAnswer(allQuestions[x]["answer"].ToString());

            questions.Add(ques);

        }

        //room1Status = room1.GetComponent<Room>().operational;
        //room2Status = room2.GetComponent<Room>().operational;
        //room3Status = room3.GetComponent<Room>().operational;

        //SceneManager.LoadScene("Quiz");

        return questions;
    }

    private JObject LoadLessonData()
    {
        JObject lesson;
        string filePath = Path.Combine(Application.streamingAssetsPath, lessonDataFileName);

        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            string dataAsJson = File.ReadAllText(filePath);

            lesson = JObject.Parse(dataAsJson);

        }
        else
        {
            lesson = null;
            Debug.LogError("Cannot load lesson data!");
        }

        return lesson;
    }

    public Lesson LoadLesson(string subject, string level)
    {
        Debug.Log("Starting Console with Subject: " + subject + " and Level: " + level);
        // Load Data from JSON with subject and level
        JObject lessonJObject = LoadLessonData();

        // Find the subject and level, load them into a JArray
        JToken lessonData = lessonJObject["lesson"][subject][level];

        JArray lessonQues = (JArray)lessonJObject["lesson"][subject][level]["quiz"];

        Lesson lesson = new Lesson();

        lesson.SetTitle(lessonData["title"].ToString());

        JArray content = (JArray)lessonData["content"];
        lesson.SetContent(content);

        for (int x = 0; x < 2; x++)
        {
            Question ques = new Question();

            ques.SetTitle(lessonQues[x]["title"].ToString());
            ques.SetContent(lessonQues[x]["content"].ToString());

            JArray choices = (JArray)lessonQues[x]["choices"];
            ques.SetChoices(choices);

            ques.SetAnswer(lessonQues[x]["answer"].ToString());

            lesson.AddQuestions(ques);
        }

        return lesson;
    }
}
