﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayStoryVideo : MonoBehaviour
{
    public RawImage rawImage;
    public VideoPlayer videoPlayer;

    private GameObject consoleQuiz;


    // Start is called before the first frame update
    void Start()
    {
        consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");
        if(consoleQuiz.GetComponent<ConsoleQuizController>().firstInstantiated){
            consoleQuiz.GetComponent<ConsoleQuizController>().firstInstantiated = false;
            StartCoroutine(PlayVideo());
        } else {
            gameObject.SetActive(false);
        }
        
    }

    private IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);

        while (!videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
        }

        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();

        while (videoPlayer.isPlaying)
        {
            yield return waitForSeconds;
        }

        gameObject.SetActive(false);
    }
}
