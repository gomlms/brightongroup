﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Room : MonoBehaviour {
    public string subject;
    public string level;
    public bool operational;
    public string roomName;

    private GameObject consoleQuiz;
    public GameObject fire;
    public GameObject upgrade;

    public bool isUpgradable = false;
    public int lessonNum;

    void Start(){

        fire.SetActive(false);

        if(consoleQuiz == null){
            consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");
        }
        operational = true;
    }

    private void Update()
    {
        if (!operational)
        {
            fire.SetActive(true);
            isUpgradable = false;
        }
    }

    public void ShowUpgrade()
    {
        upgrade.SetActive(true);
    }

    void OnTriggerEnter2D(Collider2D col){
        if(!operational){
            // Get all of the questions for that Subject and Level 
            consoleQuiz.GetComponent<ConsoleQuizController>().LoadQuestions(subject, level);
        }
    }

    void OnMouseDown(){
        if(!operational){
            // Get all of the questions for that Subject and Level 
            consoleQuiz.GetComponent<ConsoleQuizController>().LoadQuestions(subject, level);
        } else if(isUpgradable){
            GameObject consoleQuiz = GameObject.FindGameObjectWithTag("GameManager");
            consoleQuiz.GetComponent<ConsoleQuizController>().firstInstantiated = false;
            consoleQuiz.GetComponent<ConsoleQuizController>().lessonNum = lessonNum;
            SceneManager.LoadScene("LessonTest");
        }
    }
}
