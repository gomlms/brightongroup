﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TravelLesson : MonoBehaviour
{
    public int lessonNum;

    // Start is called before the first frame update
    void Start()
    {
        //Creates a 50/50 chance the user will be able to return to a base for upgrades
        float rand = Random.Range(1,9);
        if (rand <= 7){
             gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.transform.parent){
            GameObject parent = gameObject.transform.parent.gameObject;
            parent.GetComponent<Room>().isUpgradable = true;
            parent.GetComponent<Room>().lessonNum = lessonNum;
        }
    }

    void OnMouseDown(){
        //Play animation of ship leaving
    }
}
