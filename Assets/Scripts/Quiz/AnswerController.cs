﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerController : MonoBehaviour
{
  
  public string questionText;
  private GameObject GUIPrefab;
  // Start is called before the first frame update
  void Start(){
      if(!GUIPrefab){
        GUIPrefab = GameObject.FindGameObjectWithTag("GUIController");
      }
  }

  // Update is called once per frame
  void Update(){
    
  }

  void OnMouseDown(){
    GUIPrefab.GetComponent<GUIController>().checkAnswer(questionText);
  }
}
